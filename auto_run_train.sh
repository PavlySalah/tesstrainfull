#!/bin/bash
export PYTHONIOENCODING=utf8
# ulimit -s 65536  
SCRIPTPATH=`pwd`


### Parameters
MODEL=my_ara_img    # Model name
NUM_ITERS=200       # Number of iterations


# Clean any previously generated files
make clean MODEL_NAME=$MODEL    


# Copy necessary files to `data` folder
mkdir -p data

cp langdata_lstm/Arabic.unicharset  data/Arabic.unicharset
# cp langdata_lstm/Latin.unicharset  data/Latin.unicharset
# cp langdata_lstm/Hebrew.unicharset  data/Hebrew.unicharset

mkdir -p data/$MODEL
cp langdata_lstm/ara/ara.punc data/$MODEL/$MODEL.punc
cp langdata_lstm/ara/ara.numbers data/$MODEL/$MODEL.numbers
cp langdata_lstm/ara/ara.config data/$MODEL/$MODEL.config
cp langdata_lstm/ara/ara.wordlist data/$MODEL/$MODEL.wordlist


# Start training
echo "Started Training..."
make training MODEL_NAME=$MODEL LANG_TYPE=RTL \
    START_MODEL=Arabic TESSDATA=tesseract/tessdata/script/ \
    DEBUG_INTERVAL=-1 MAX_ITERATIONS=$NUM_ITERS
